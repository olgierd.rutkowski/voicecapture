// Fill out your copyright notice in the Description page of Project Settings.


#include "VoiceCaptureComponent.h"


UVoiceCaptureComponent::UVoiceCaptureComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

bool UVoiceCaptureComponent::StartCapture()
{
	if (VoiceCapture->IsCapturing())
	{
		UE_LOG(LogTemp, Warning, TEXT("Capturing has already started, won't start another one."));
		return false;
	}
	UE_LOG(LogTemp, Log, TEXT("Started capturing."));
	VoiceCaptureSoundWave->ResetAudio();
	VoiceCaptureLongBuffer.Reset();
	return VoiceCapture->Start();
}

void UVoiceCaptureComponent::StopCapture()
{
	if ( ! VoiceCapture->IsCapturing())
	{
		UE_LOG(LogTemp, Warning, TEXT("No capture has been started, can't stop, won't stop."));
		return;
	}
	UE_LOG(LogTemp, Log, TEXT("Stopped capturing."));
	VoiceCapture->Stop();
}

void UVoiceCaptureComponent::PlaySound(USoundWave* SoundToPlay)
{
	VoiceCaptureOutput->SetSound(SoundToPlay);
	VoiceCaptureOutput->Play();
}

void UVoiceCaptureComponent::SetPlaybackVolume(const float Volume)
{
	if (Volume < 0.0f || Volume > 1.0f)
	{
		UE_LOG(LogTemp, Error, TEXT("Volume should be between 0.0 and 1.0."));
		return;
	}

	const float MultipliedVolume{DefaultVolume * Volume};
	UE_LOG(LogTemp, Log, TEXT("Setting new volume for playback: %0.1f."), MultipliedVolume)
	VoiceCaptureOutput->VolumeMultiplier = MultipliedVolume;
	VoiceCaptureSoundWave->Volume = MultipliedVolume;
}

void UVoiceCaptureComponent::BeginPlay()
{
	Super::BeginPlay();

	// Passing empty string causes to retrieve the default audio input device, which seems like the most plausible solution.
	VoiceCapture = FVoiceModule::Get().CreateVoiceCapture(FString{});
	if ( ! VoiceCapture.IsValid())
	{
		UE_LOG(LogTemp, Error, TEXT("The default audio input device is invalid. Plugin won't work."));
		return;
	}

	AActor* Owner{GetOwner()};
	VoiceCaptureOutput = Cast<UAudioComponent>(
		Owner->AddComponentByClass(
			UAudioComponent::StaticClass(),
			false,
			FTransform::Identity,
			false
		)
	);
	
	VoiceCaptureOutput->bAutoActivate = true;
	VoiceCaptureOutput->bCanPlayMultipleInstances = false;
	VoiceCaptureOutput->VolumeMultiplier = DefaultVolume;

	VoiceCaptureSoundWave = NewObject<USoundWaveProcedural>();
	VoiceCaptureSoundWave->SetSampleRate(16000);
	VoiceCaptureSoundWave->NumChannels = 1;
	VoiceCaptureSoundWave->Duration = INDEFINITELY_LOOPING_DURATION;
	VoiceCaptureSoundWave->bLooping = false;
	VoiceCaptureSoundWave->bProcedural = true;
	VoiceCaptureSoundWave->Volume = DefaultVolume;
}

void UVoiceCaptureComponent::TickComponent(
	float DeltaTime,
	ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction
)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if ( ! VoiceCapture.IsValid())
	{
		return;
	}

	uint32 VoiceCaptureAvailableBytes{0u};
	const EVoiceCaptureState::Type CaptureState{VoiceCapture->GetCaptureState(VoiceCaptureAvailableBytes)};
	if ( ! VoiceCapture->IsCapturing() && CaptureState != EVoiceCaptureState::Stopping)
	{
		return;
	}
	
	if (CaptureState != EVoiceCaptureState::Ok && CaptureState != EVoiceCaptureState::Stopping)
	{
		return;
	}

	uint32 VoiceCaptureReadBytes{0u};
	TArray<uint8> VoiceCaptureBuffer;
	VoiceCaptureBuffer.SetNumUninitialized(VoiceCaptureAvailableBytes);
	VoiceCapture->GetVoiceData(
		VoiceCaptureBuffer.GetData(),
		VoiceCaptureAvailableBytes,
		VoiceCaptureReadBytes
	);
	VoiceCaptureLongBuffer.Append(VoiceCaptureBuffer.GetData(), VoiceCaptureReadBytes);

	if (CaptureState == EVoiceCaptureState::Stopping)
	{
		VoiceCaptureSoundWave->ResetAudio();
		VoiceCaptureSoundWave->QueueAudio(VoiceCaptureLongBuffer.GetData(), VoiceCaptureLongBuffer.Num());
		UE_LOG(LogTemp, Warning, TEXT("Broadcasting captured sound."));
		CapturedSoundDelegate.Broadcast(VoiceCaptureSoundWave);
	}
}

