// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Components/ActorComponent.h"
#include "Sound/SoundWaveProcedural.h"
#include "Voice.h"
#include "VoiceCaptureComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE_OneParam(
	FCapturedSoundWave,
	UVoiceCaptureComponent,
	CapturedSoundDelegate,
	USoundWave*,
	CapturedSound
);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VOICECAPTURECOMPONENTPLUGIN_API UVoiceCaptureComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVoiceCaptureComponent();
	
	UFUNCTION(BlueprintCallable)
	bool StartCapture();

	UFUNCTION(BlueprintCallable)
	void StopCapture();

	UFUNCTION(BlueprintCallable)
	void PlaySound(USoundWave* SoundToPlay);

	UFUNCTION(BlueprintCallable)
	void SetPlaybackVolume(const float Volume);

	UPROPERTY(BlueprintAssignable)
	FCapturedSoundWave CapturedSoundDelegate;
	
	UPROPERTY()
	UAudioComponent* VoiceCaptureOutput;

	UPROPERTY()
	USoundWaveProcedural* VoiceCaptureSoundWave;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	const float DefaultVolume{4.5f};
	
	TSharedPtr<IVoiceCapture> VoiceCapture{nullptr};
	TArray<uint8> VoiceCaptureLongBuffer;

	bool IsCapturing{false};

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
