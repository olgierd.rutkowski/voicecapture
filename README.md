# VoiceCapture

A test task to create voice capture plugin for Unreal Engine 5.
Plugin is attached to the player pawn, can record and playback the sound.
Record and playback is controlled via on-screen widgets and keyboard shortcuts.
"A" to start recording. "S" to stop and playback the recording.

Plugin is written in C++, while most of the actor and widget is done using blueprints.


## Getting started

Solution has been done on the current Unreal Engine 5 release: 5.2.1 without any additional 3rd party plugins.
Voice capture plugin uses "Voice" component.
