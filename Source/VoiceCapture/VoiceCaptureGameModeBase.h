// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VoiceCaptureGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VOICECAPTURE_API AVoiceCaptureGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
